<?php
$server = 'localhost';
$username = 'root';
$password = 'root';
$database = 'animaldom';

try{
	$conn = new PDO("mysql:host=$server;dbname=$database;", $username, $password);
} catch(PDOException $e){
	die( "Connection failed: " . $e->getMessage());
}
?>